import config from '@/services';
import axios, { AxiosResponse } from 'axios';

export default class Course {
  static get(page: number, perPage: number): Promise<AxiosResponse> {
    return axios.get(`${config.base}/s/course?page=${page}&per_page=${perPage}`);
  }

  static getById(id: number): Promise<AxiosResponse> {
    return axios.get(`${config.base}/s/course/${id}`);
  }

  static leave(id: number): Promise<AxiosResponse> {
    return axios.delete(`${config.base}/s/course/join/${id}`);
  }
}
