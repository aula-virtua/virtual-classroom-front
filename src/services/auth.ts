import { AxiosResponse } from 'axios';
import axios from '@/services/axios';

class Auth {
  static forgot(email: string): Promise<AxiosResponse> {
    return axios.post(`/auth/forgot`, {
      email,
    });
  }

  static login(code: string, password: string): Promise<AxiosResponse> {
    return axios.post(`/auth/login`, {
      code,
      password,
    });
  }

  static register(
    code: string,
    name: string,
    first_last_name: string,
    second_last_name: string,
    email: string,
    password: string,
    password_confirmation: string,
  ): Promise<AxiosResponse> {
    return axios.post('/auth/register', {
      code,
      name,
      first_last_name,
      second_last_name,
      email,
      password,
      password_confirmation,
    });
  }

  static reset(
    password: string,
    password_confirmation: string,
    token: string,
  ): Promise<AxiosResponse> {
    return axios.post('/auth/reset', {
      password,
      password_confirmation,
      token,
    });
  }

  static;
}

export default Auth;
