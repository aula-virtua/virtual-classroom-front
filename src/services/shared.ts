import axios, { AxiosResponse } from 'axios';
import config from './index';

export default class Shared {
  static profile(): Promise<AxiosResponse> {
    return axios.get(`${config.base}/sh/auth`);
  }

  static logout(): Promise<AxiosResponse> {
    return axios.get(`${config.base}/sh/auth/logout`);
  }

  static updateProfile(
    email: string,
    name: string,
    first_last_name: string,
    second_last_name: string,
  ): Promise<AxiosResponse> {
    return axios.post(`${config.base}/sh/auth`, {
      email,
      name,
      first_last_name,
      second_last_name,
    });
  }

  static resetPassword(
    current_password: string,
    password: string,
    password_confirmation: string,
  ): Promise<AxiosResponse> {
    return axios.post(`${config.base}/sh/auth/password`, {
      current_password,
      password,
      password_confirmation,
    });
  }
}
