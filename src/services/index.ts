export default {
  base: `${process.env.VUE_APP_ROOT_API}/${process.env.VUE_APP_VERSION_API}`,
  no: `${process.env.VUE_APP_ROOT_API}`,
};
