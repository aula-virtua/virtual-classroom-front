import axios from 'axios';

const instance = axios.create({
  baseURL: `${process.env.VUE_APP_ROOT_API}/${process.env.VUE_APP_VERSION_API}`,
});

export default instance;
