import config from '@/services';
import axios, { AxiosResponse } from 'axios';

export default class Course {
  static get(page: number, perPage: number): Promise<AxiosResponse> {
    return axios.get(`${config.base}/t/course?page=${page}&per_page=${perPage}`);
  }

  static getById(id: number): Promise<AxiosResponse> {
    return axios.get(`${config.base}/t/course/${id}`);
  }

  static course(code: string, name: string, description: string): Promise<AxiosResponse> {
    return axios.post(`${config.base}/t/course`, {
      code,
      name,
      description,
    });
  }
}
