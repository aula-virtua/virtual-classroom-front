import axios, { AxiosResponse } from 'axios';
import config from '@/services';

export default class Homework {
  static get(id: number, page: number, perPage: number): Promise<AxiosResponse> {
    return axios.get(`${config.base}/t/course/${id}/homework?page=${page}&per_page=${perPage}`);
  }
}
