import Vue from 'vue';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import {
  faCogs,
  faSignOutAlt,
  faPlus,
  faSearch,
  faHome,
  faSquare,
  faCheckSquare,
  faQuestion,
  faKey,
  faLock,
  faRedoAlt,
  faAt,
  faUserAlt,
  faChalkboardTeacher,
  faPencilAlt,
  faHashtag,
  faTimes,
  faImage,
} from '@fortawesome/free-solid-svg-icons';
import { faClock } from '@fortawesome/free-regular-svg-icons';
import DefaultLayout from './layouts/Default.vue';
import StudentLayout from './layouts/Student.vue';
import TeacherLayout from './layouts/Teacher.vue';
import App from './App.vue';
import router from './router';
import store from './store';

// Icons to use in the application
library.add(
  faCogs,
  faSignOutAlt,
  faPlus,
  faSearch,
  faHome,
  faSquare,
  faCheckSquare,
  faQuestion,
  faKey,
  faLock,
  faRedoAlt,
  faAt,
  faUserAlt,
  faChalkboardTeacher,
  faPencilAlt,
  faHashtag,
  faTimes,
  faClock,
  faImage,
);

Vue.component('icon', FontAwesomeIcon);
Vue.component('default-layout', DefaultLayout);
Vue.component('student-layout', StudentLayout);
Vue.component('teacher-layout', TeacherLayout);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
