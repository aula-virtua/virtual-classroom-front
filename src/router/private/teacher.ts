import { RouteConfig } from 'vue-router';

const prefix = '/t';

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'teacher-dashboard',
    component: () => import('@/views/private/teacher/Home.vue'),
  },
  {
    path: '/edit',
    name: 'teacher-profile',
    component: () => import('@/views/public/Edit.vue'),
  },
  {
    path: '/create',
    name: 'create-course',
    component: () => import('@/views/private/teacher/Create.vue'),
  },
  {
    path: '/course/:id',
    name: 'teacher-course',
    component: () => import('@/views/private/teacher/Course.vue'),
    children: [
      {
        path: '/homework/:id',
        name: 'teacher-homework',
        component: () => import('@/views/private/teacher/Deliver.vue'),
      },
      // {
      //   path: '/forum/:id',
      //   name: 'teacher-forum',
      //   component: () => import('@/views/private/teacher/Forum.vue'),
      // },
    ],
  },
];

export default routes.map((route) => ({
  ...route,
  path: `${prefix}${route.path}`,
  meta: {
    requiresAuth: true,
    type: '2',
    layout: 'teacher',
  },
}));
