import { RouteConfig } from 'vue-router';
import administratorRoutes from './administrator';
import teacherRoutes from './teacher';
import studentRoutes from './student';

const routes: Array<RouteConfig> = [
  ...administratorRoutes,
  ...teacherRoutes,
  ...studentRoutes,
];

export default routes;
