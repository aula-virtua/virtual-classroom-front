import { RouteConfig } from 'vue-router';

const prefix = '/s';

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'student-dashboard',
    component: () => import('@/views/private/student/Home.vue'),
  },
  {
    path: '/edit',
    name: 'student-profile',
    component: () => import('@/views/public/Edit.vue'),
  },
  {
    path: '/course/:id',
    name: 'student-course',
    component: () => import('@/views/private/student/Course.vue'),
    children: [
      {
        path: '/forum/:fid',
        name: 'student-forum',
        component: () => import('@/views/private/student/Forum.vue'),
      },
    ],
  },
  {
    path: '/course/:id/homework/:hid',
    name: 'student-homework',
    component: () => import('@/views/private/student/Deliver.vue'),
  },
];

export default routes.map((route) => ({
  ...route,
  path: `${prefix}${route.path}`,
  meta: {
    requiresAuth: true,
    type: '3',
    layout: 'student',
  },
}));
