import { RouteConfig } from 'vue-router';

const prefix = '/a';

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'admin-dashboard',
    component: () => import('@/views/private/administrator/Home.vue'),
  },
];

export default routes.map((route) => ({
  ...route,
  path: `${prefix}${route.path}`,
  meta: {
    requiresAuth: true,
    type: '1',
    layout: 'sidebar',
  },
}));
