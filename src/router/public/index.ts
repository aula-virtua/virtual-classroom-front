import { RouteConfig } from 'vue-router';
import Home from '@/views/public/Home.vue';

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/reset/:token',
    name: 'reset-password',
    component: () => import('@/views/public/Reset.vue'),
  },
];

export default routes.map((route) => ({
  ...route,
  meta: {
    requiresAuth: undefined,
  },
}));
