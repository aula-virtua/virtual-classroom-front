import { RouteConfig } from 'vue-router';

const routes: Array<RouteConfig> = [
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/session/Login.vue'),
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('@/views/session/Register.vue'),
  },
  {
    path: '/forgot',
    name: 'Forgot',
    component: () => import('@/views/session/Forgot.vue'),
  },
];

export default routes.map((route) => ({
  ...route,
  meta: {
    requiresAuth: false,
  },
}));
