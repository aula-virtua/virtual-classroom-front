import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '@/store';
import { MUTATIONS as AuthMutations, ACTIONS as AuthActions } from '@/store/modules/auth';
import { toBaseURL } from '@/utils/auth';
import { UserType } from '@/interfaces';
import sessionRoutes from './session';
import publicRoutes from './public';
import privateRoutes from './private';

const routes = [...sessionRoutes, ...publicRoutes, ...privateRoutes];

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (!store.state.auth.loaded) {
    store.commit(AuthMutations.LOAD_TOKEN);
    if (store.state.auth.token) {
      store.dispatch(AuthActions.FETCH_PROFILE);
    }
  }

  if (to.matched.some((record) => record.meta.requiresAuth)) {
    const { token } = store.state.auth;

    if (token) {
      const { type } = store.state.auth;
      if (to.matched.some((record) => record.meta.type === type)) {
        next();
      } else {
        const dashboard = toBaseURL(type as UserType);
        next({ path: dashboard });
      }
    } else {
      next({ path: '/login', params: { nextUrl: to.fullPath } });
    }
  } else if (to.matched.some((record) => record.meta.requiresAuth !== undefined)) {
    const { token } = store.state.auth;
    if (token) {
      const { type } = store.state.auth;
      const dashboard = toBaseURL(type as UserType);
      next({ path: dashboard });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
