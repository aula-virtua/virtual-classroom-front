import { HomeworkModule, StoreDefinition, HomeworkPreview } from '@/interfaces';
import { ActionContext } from 'vuex';
import homeworkService from '@/services/student/homework';

const initialState = {
  perPage: 6,
  previews: {},
} as HomeworkModule;

export const MUTATIONS = {
  HAPPEND_PREVIEWS: 'HAPPEND_PREVIEWS',
  HINCREASE_PAGE: 'HINCREASE_PAGE',
  HREMAINED: 'HREMAINED',
  HINITIAL_FETCH: 'HINITIAL_FETCH',
  RESET_HOMEWORK_MODULE: 'RESET_HOMEWORK_MODULE',
};

const mutations = {
  [MUTATIONS.HAPPEND_PREVIEWS](
    state: HomeworkModule,
    payload: {
      id: number;
      previews: HomeworkPreview[];
    },
  ) {
    state.previews[payload.id].data = [
      ...(state.previews[payload.id]?.data || []),
      ...payload.previews,
    ];
  },
  [MUTATIONS.HINCREASE_PAGE](state: HomeworkModule, id: number) {
    state.previews[id].page += 1;
  },
  [MUTATIONS.HREMAINED](
    state: HomeworkModule,
    payload: {
      id: number;
      remained: boolean;
    },
  ) {
    state.previews[payload.id].remained = payload.remained;
  },
  [MUTATIONS.HINITIAL_FETCH](state: HomeworkModule, id: number) {
    state.previews[id].initialFetch = true;
  },
  [MUTATIONS.RESET_HOMEWORK_MODULE](state: HomeworkModule) {
    state.previews = {};
    state.perPage = 10;
  },
};

export const ACTIONS = {
  FETCH_HOMEWORKS: 'FETCH_HOMEWORKS',
};

const actions = {
  async [ACTIONS.FETCH_HOMEWORKS](
    context: ActionContext<HomeworkModule, StoreDefinition>,
    id: number,
  ) {
    try {
      if (!context.state.previews[id]) {
        // context.state.previews[id].page = 1;
      }
      const page = context.state.previews[id]?.page || 1;
      const response = await homeworkService.get(id, page, context.state.perPage);
      const body = response.data;
      const remained = body.links.next !== null;

      context.commit(MUTATIONS.HAPPEND_PREVIEWS, { id, previews: body.data });
      context.commit(MUTATIONS.HINCREASE_PAGE, { id });
      context.commit(MUTATIONS.HREMAINED, { id, remained });
    } catch (err) {
      // TODO: Handler
    }
  },
};

const getters = {};

const store = {
  state: initialState,
  mutations,
  actions,
  getters,
};

export default store;
