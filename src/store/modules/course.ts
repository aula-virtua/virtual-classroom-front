import {
  CourseModule,
  StoreDefinition,
  CoursePreView,
  CourseView,
  HomeworkPreview,
  SHomeworkView,
} from '@/interfaces';
import courseService from '@/services/student/course';
import homeworkService from '@/services/student/homework';
import { ActionContext } from 'vuex';

const initialState = {
  page: 1,
  previews: [],
  remained: true,
  perPage: 4,
  initialFetch: false,
  courses: {},
} as CourseModule;

export const MUTATIONS = {
  APPEND_PREVIEWS: 'APPEND_PREVIEWS',
  INCREASE_PAGE: 'INCREASE_PAGE',
  REMAINED: 'REMAINED',
  INITIAL_FETCH: 'INITIAL_FETCH',
  HINITIAL_FETCH: 'HINITIAL_FETCH',
  HINCREASE_PAGE: 'HINCREASE_PAGE',
  HREMAINED: 'HREMAINED',
  SET_COURSE: 'SET_COURSE',
  SET_HOMEWORK: 'SET_HOMEWORK',
  RESET_COURSE_MODULE: 'RESET_COURSE_MODULE',
  APPEND_HOMEWORKS: 'APPEND_HOMEWOKRS',
  REMOVE_COURSE: 'REMOVE_COURSE',
};

export const ACTIONS = {
  FETCH_COURSES: 'FETCH_COURSES',
  FETCH_COURSE: 'FETCH_COURSE',
  FETCH_HOMEWORKS: 'FETCH_HOMEWORKS',
  FETCH_HOMEWORK: 'FETCH_HOMEWORK',
};

const actions = {
  async [ACTIONS.FETCH_COURSES](context: ActionContext<CourseModule, StoreDefinition>) {
    try {
      const response = await courseService.get(context.state.page, context.state.perPage);
      const body = response.data;
      const remained = body.links.next !== null;

      context.commit(MUTATIONS.APPEND_PREVIEWS, body.data);
      context.commit(MUTATIONS.INCREASE_PAGE);
      context.commit(MUTATIONS.REMAINED, remained);
    } catch (err) {
      // TODO: Handle
    }
  },

  async [ACTIONS.FETCH_COURSE](context: ActionContext<CourseModule, StoreDefinition>, id: number) {
    try {
      const response = await courseService.getById(id);

      context.commit(MUTATIONS.SET_COURSE, response.data);
    } catch (err) {
      //
    }
  },

  async [ACTIONS.FETCH_HOMEWORKS](
    context: ActionContext<CourseModule, StoreDefinition>,
    id: number,
  ) {
    try {
      const { page, perPage } = context.state.courses[id].h;
      const response = await homeworkService.get(id, page, perPage);
      const body = response.data;
      const remained = body.links.next !== null;

      let previews = body.data as HomeworkPreview[];
      previews = previews.map((h) => ({
        ...h,
        deliveryDate: new Date(h.deliveryDate),
      }));
      context.commit(MUTATIONS.APPEND_HOMEWORKS, { id, previews });
      context.commit(MUTATIONS.HINCREASE_PAGE, id);
      context.commit(MUTATIONS.HREMAINED, { id, remained });
    } catch (err) {
      // TODO: Handle
    }
  },

  async [ACTIONS.FETCH_HOMEWORK](
    context: ActionContext<CourseModule, StoreDefinition>,
    payload: {
      id: number;
      hid: number;
    },
  ) {
    try {
      const response = await homeworkService.getById(payload.id, payload.hid);
      context.commit(MUTATIONS.SET_HOMEWORK, { homework: response.data, id: payload.id });
    } catch (err) {
      // TODO: Handle
    }
  },
};

const mutations = {
  [MUTATIONS.APPEND_PREVIEWS](state: CourseModule, previews: CoursePreView[]) {
    state.previews = [...state.previews, ...previews];
  },
  [MUTATIONS.INCREASE_PAGE](state: CourseModule) {
    state.page += 1;
  },
  [MUTATIONS.HINCREASE_PAGE](state: CourseModule, id: number) {
    state.courses[id].h.page += 1;
  },
  [MUTATIONS.REMAINED](state: CourseModule, remained: boolean) {
    state.remained = remained;
  },
  [MUTATIONS.INITIAL_FETCH](state: CourseModule) {
    state.initialFetch = true;
  },
  [MUTATIONS.HINITIAL_FETCH](state: CourseModule, id: number) {
    state.courses[id].h.initialFetch = true;
  },
  [MUTATIONS.HREMAINED](
    state: CourseModule,
    payload: {
      id: number;
      remained: boolean;
    },
  ) {
    state.courses[payload.id].h.remained = payload.remained;
  },
  [MUTATIONS.SET_COURSE](state: CourseModule, course: CourseView) {
    state.courses[course.id] = {
      ...course,
      h: {
        homeworks: {},
        previews: [],
        initialFetch: false,
        page: 1,
        perPage: 5,
        remained: true,
      },
    };
  },
  [MUTATIONS.SET_HOMEWORK](
    state: CourseModule,
    payload: {
      id: number;
      homework: SHomeworkView;
    },
  ) {
    const { id, homework } = payload;
    state.courses[id].h.homeworks[homework.id] = {
      ...homework,
      deliveryDate: new Date(homework.deliveryDate),
    };
  },
  [MUTATIONS.APPEND_HOMEWORKS](
    state: CourseModule,
    payload: {
      id: number;
      previews: HomeworkPreview[];
    },
  ) {
    state.courses[payload.id].h.previews = payload.previews;
  },
  [MUTATIONS.RESET_COURSE_MODULE](state: CourseModule) {
    state.courses = {};
    state.previews = [];
    state.initialFetch = false;
    state.page = 1;
    state.perPage = 10;
    state.remained = true;
  },
  [MUTATIONS.REMOVE_COURSE](state: CourseModule, id: number) {
    state.previews = state.previews.filter((p) => p.id !== id);
    delete state.courses[id];
  },
};

const store = {
  state: initialState,
  mutations,
  actions,
};

export default store;
