import { MessageModule, PopperMessage } from '@/interfaces';

const initialState = {
  messages: [],
} as MessageModule;

export const MUTATIONS = {
  ADD_MESSAGE: 'ADD_MESSAGE',
  REMOVE_MESSAGE: 'REMOVE_MESSAGE',
};

const mutations = {
  [MUTATIONS.ADD_MESSAGE](state: MessageModule, message: PopperMessage) {
    state.messages = [...state.messages, message];
  },
  [MUTATIONS.REMOVE_MESSAGE](state: MessageModule, id: string) {
    state.messages = state.messages.filter((m) => m.id !== id);
  },
};

const store = {
  state: initialState,
  mutations,
};

export default store;
