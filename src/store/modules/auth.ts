import decode from 'jwt-decode';
import axios from 'axios';
import { User, AuthModule, StoreDefinition } from '@/interfaces';
import { ActionContext } from 'vuex';
import sharedService from '@/services/shared';

export const MUTATIONS = {
  LOAD_TOKEN: 'LOAD_TOKEN',
  SET_TOKEN: 'SET_TOKEN',
  RESET_TOKEN: 'RESET_TOKEN',
  SET_USER: 'SET_USER',
  RELOAD: 'RELOAD',
};

export const ACTIONS = {
  FETCH_PROFILE: 'FETCH_PROFILE',
  LOGOUT: 'LOGOUT',
};

const initialState = {
  loaded: false,
  token: null,
  user: {
    loaded: false,
    profile: {},
  },
  type: null,
} as AuthModule;

const getters = {
  fullName: (state: AuthModule) => {
    if (state.user.loaded) {
      const { name, firstLastName, secondLastName } = state.user.profile;

      return `${name} ${firstLastName} ${secondLastName}`;
    }

    return '';
  },
};

const actions = {
  async [ACTIONS.FETCH_PROFILE](context: ActionContext<AuthModule, StoreDefinition>) {
    try {
      const response = await sharedService.profile();
      context.commit(MUTATIONS.SET_USER, response.data);
    } catch (err) {
      console.log(err.response.data);
      // TODO
    }
  },
  async [ACTIONS.LOGOUT](context: ActionContext<AuthModule, StoreDefinition>) {
    try {
      await sharedService.logout();
      context.commit(MUTATIONS.RESET_TOKEN);
    } catch (err) {
      // TODO
      console.log(err.response.data);
    }
  },
};

const mutations = {
  [MUTATIONS.SET_TOKEN](state: AuthModule, token: string) {
    const data = decode(token) as Record<string, any>;
    state.type = data.typ;
    state.token = token;
    localStorage.setItem('token', token);
    axios.defaults.headers.common.Authorization = `Bearer ${token}`;
  },

  [MUTATIONS.LOAD_TOKEN](state: AuthModule) {
    const token = localStorage.getItem('token');
    if (token) {
      const content = decode(token) as Record<string, any>;
      state.type = content.typ;
      state.token = token;
      axios.defaults.headers.common.Authorization = `Bearer ${token}`;
    }
    state.loaded = true;
  },

  [MUTATIONS.RESET_TOKEN](state: AuthModule) {
    state.token = null;
    state.type = null;
    state.user = {
      loaded: false,
      profile: {} as User,
    };
    localStorage.removeItem('token');
    axios.defaults.headers.common.Authorization = null;
  },

  [MUTATIONS.SET_USER](state: AuthModule, profile: User) {
    state.user = {
      loaded: true,
      profile,
    };
  },

  [MUTATIONS.RELOAD](
    state: AuthModule,
    data: { email: string; name: string; firstLastName: string; secondLastName: string },
  ) {
    state.user.profile.email = data.email;
    state.user.profile.name = data.name;
    state.user.profile.firstLastName = data.firstLastName;
    state.user.profile.secondLastName = data.secondLastName;
  },
};

const store = {
  state: initialState,
  getters,
  mutations,
  actions,
};

export default store;
