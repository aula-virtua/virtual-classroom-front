import {
  CourseTModule,
  StoreDefinition,
  CoursePreviewT,
  CourseTeacher,
  CourseInformation,
} from '@/interfaces';
import courseService from '@/services/teacher/course';
import { ActionContext } from 'vuex';

const initialState = {
  page: 1,
  previews: [],
  remained: true,
  perPage: 4,
  initialFetch: false,
  courses: {},
} as CourseTModule;

export const MUTATIONS = {
  APPEND_PREVIEWS: 'APPEND_PREVIEWS',
  INCREASE_PAGE: 'INCREASE_PAGE',
  REMAINED: 'REMAINED',
  INITIAL_FETCH: 'INITIAL_FETCH',
  HINITIAL_FETCH: 'HINITIAL_FETCH',
  HINCREASE_PAGE: 'HINCREASE_PAGE',
  HREMAINED: 'HREMAINED',
  SET_COURSE: 'SET_COURSE',
  SET_HOMEWORK: 'SET_HOMEWORK',
  RESET_COURSE_MODULE: 'RESET_COURSE_MODULE',
  APPEND_HOMEWORKS: 'APPEND_HOMEWOKRS',
  REMOVE_COURSE: 'REMOVE_COURSE',
  RELOAD: 'RELOAD',
};

export const ACTIONS = {
  FETCH_COURSES: 'FETCH_COURSES',
  FETCH_COURSE: 'FETCH_COURSE',
};

const actions = {
  async [ACTIONS.FETCH_COURSES](context: ActionContext<CourseTModule, StoreDefinition>) {
    try {
      const response = await courseService.get(context.state.page, context.state.perPage);
      const body = response.data;
      const remained = body.links.next !== null;

      context.commit(MUTATIONS.APPEND_PREVIEWS, body.data);
      context.commit(MUTATIONS.INCREASE_PAGE);
      context.commit(MUTATIONS.REMAINED, remained);
    } catch (err) {
      // TODO: Handle
    }
  },

  async [ACTIONS.FETCH_COURSE](context: ActionContext<CourseTModule, StoreDefinition>, id: number) {
    try {
      const response = await courseService.getById(id);

      context.commit(MUTATIONS.SET_COURSE, response.data);
    } catch (err) {
      //
    }
  },
};

const mutations = {
  [MUTATIONS.APPEND_PREVIEWS](state: CourseTModule, previews: CoursePreviewT[]) {
    state.previews = [...state.previews, ...previews];
  },
  [MUTATIONS.INCREASE_PAGE](state: CourseTModule) {
    state.page += 1;
  },
  [MUTATIONS.REMAINED](state: CourseTModule, remained: boolean) {
    state.remained = remained;
  },
  [MUTATIONS.INITIAL_FETCH](state: CourseTModule) {
    state.initialFetch = true;
  },
  [MUTATIONS.SET_COURSE](state: CourseTModule, course: CourseTeacher) {
    state.courses[course.id] = course;
  },
  [MUTATIONS.RESET_COURSE_MODULE](state: CourseTModule) {
    state.courses = {};
    state.previews = [];
    state.initialFetch = false;
    state.page = 1;
    state.perPage = 10;
    state.remained = true;
  },
  [MUTATIONS.RELOAD](state: CourseTModule, data: CoursePreviewT) {
    state.previews.push(data);
  },
};

const store = {
  state: initialState,
  mutations,
  actions,
};

export default store;
