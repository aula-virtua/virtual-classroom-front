import Vue from 'vue';
import Vuex from 'vuex';

import { StoreDefinition } from '@/interfaces';
import auth from './modules/auth';
import scourse from './modules/course';
import message from './modules/message';
import tcourse from './modules/tcourse';

Vue.use(Vuex);

const state = new Vuex.Store<StoreDefinition>({
  modules: {
    auth,
    scourse,
    message,
    tcourse,
  },
});

export default state;
