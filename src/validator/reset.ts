import validate from 'validate.js';

const constrains = {
  password: {
    length: { minimum: 10, message: 'debe contener como minimo 10 caracteres' },
  },
  verifyPassword: {
    equality: { attribute: 'password', message: 'y contraseña deben ser identicas' },
  },
};

export function validationPass(password: string, verifyPassword: string) {
  const aliases: Record<string, string> = {
    password: 'Nueva contraseña',
    verifyPassword: 'Verificar contraseña',
  };

  return validate({ password, verifyPassword }, constrains, {
    prettify: function prettify(string: string) {
      return aliases[string] || validate.prettify(string);
    },
  });
}

export default validationPass;
