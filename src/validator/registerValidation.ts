import validate from 'validate.js';

const constrains = {
  code: {
    format: {
      pattern: /[0-9]+/,
      message: 'debe ser numerico',
    },
    length: { is: 9, message: 'debe tener 9 dígitos' },
  },
  name: { length: { maximum: 40, minimum: 1, message: 'debe contener de 1 a 40 caracteres' } },
  firstLastName: {
    length: { maximum: 40, minimum: 1, message: 'debe contener de 1 a 40 caracteres' },
  },
  secondLastName: {
    length: { maximum: 40, minimum: 1, message: 'debe contener de 1 a 40 caracteres' },
  },
  email: {
    email: true,
  },
  password: {
    length: { minimum: 10, message: 'debe contener como minimo 10 caracteres' },
  },
  verifyPassword: {
    equality: { attribute: 'password', message: 'y contraseña deben ser identicas' },
  },
};

function validation(
  code: string,
  name: string,
  firstLastName: string,
  secondLastName: string,
  email: string,
  password: string,
  verifyPassword: string,
) {
  const aliases: Record<string, string> = {
    code: 'codigo',
    name: 'Nombre',
    firstLastName: 'Apellido paterno',
    secondLastName: 'Apellido materno',
    email: 'Correo electronico',
    password: 'Contraseña',
    verifyPassword: 'Verificar contraseña',
  };

  return validate(
    { code, name, firstLastName, secondLastName, email, password, verifyPassword },
    constrains,
    {
      prettify: function prettify(string: string) {
        return aliases[string] || validate.prettify(string);
      },
    },
  );
}

export default validation;
