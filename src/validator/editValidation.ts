import validate from 'validate.js';

const constrains = {
  name: { length: { maximum: 40, minimum: 1, message: 'debe contener de 1 a 40 caracteres' } },
  firstLastName: {
    length: { maximum: 40, minimum: 1, message: 'debe contener de 1 a 40 caracteres' },
  },
  secondLastName: {
    length: { maximum: 40, minimum: 1, message: 'debe contener de 1 a 40 caracteres' },
  },
  email: {
    email: true,
  },
};

export function validationIdentity(
  name: string,
  firstLastName: string,
  secondLastName: string,
  email: string,
) {
  const aliases: Record<string, string> = {
    name: 'Nombre',
    firstLastName: 'Apellido paterno',
    secondLastName: 'Apellido materno',
    email: 'Correo electronico',
  };

  return validate({ name, firstLastName, secondLastName, email }, constrains, {
    prettify: function prettify(string: string) {
      return aliases[string] || validate.prettify(string);
    },
  });
}

const constrainsPass = {
  currentPassword: {
    length: { minimum: 10, message: 'debe contener como minimo 10 caracteres' },
  },
  newPassword: {
    length: { minimum: 10, message: 'debe contener como minimo 10 caracteres' },
  },
  verifyPassword: {
    equality: { attribute: 'newPassword', message: 'y contraseña deben ser identicas' },
  },
};

export function validationPass(
  currentPassword: string,
  newPassword: string,
  verifyPassword: string,
) {
  const aliases: Record<string, string> = {
    currentPassword: 'Contraseña actual',
    newPassword: 'Nueva contraseña',
    verifyPassword: 'Verificar contraseña',
  };

  return validate({ currentPassword, newPassword, verifyPassword }, constrainsPass, {
    prettify: function prettify(string: string) {
      return aliases[string] || validate.prettify(string);
    },
  });
}
