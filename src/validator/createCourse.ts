import validate from 'validate.js';

const constrains = {
  code: { length: { is: 10, message: 'debe contener 16 caracteres' } },
  name: { length: { maximum: 40, minimum: 1, message: 'debe contener de 1 a 40 caracteres' } },
  description: {
    length: { aximum: 400, minimum: 1, message: 'debe contener de 1 a 400 caracteres' },
  },
};

export function validation(code: string, name: string, description: string) {
  const aliases: Record<string, string> = {
    code: 'codigo',
    name: 'Nombre',
    description: 'Descripción',
  };

  return validate({ code, name, description }, constrains, {
    prettify: function prettify(string: string) {
      return aliases[string] || validate.prettify(string);
    },
  });
}

export default validation;
