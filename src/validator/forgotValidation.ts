import validate from 'validate.js';

const constrains = {
  email: {
    email: true,
  },
};

function validation(email: string) {
  const aliases: Record<string, string> = {
    email: 'Correo electronico',
  };

  return validate({ email }, constrains, {
    prettify: function prettify(string: string) {
      return aliases[string] || validate.prettify(string);
    },
  });
}

export default validation;
