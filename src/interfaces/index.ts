export interface CourseStudent {
  code: string;
  id: number;
  name: string;
  description: string;
  forums: number;
  banner: string;
  student: {
    calification: number;
  };
  teacher: {
    id: number;
    picture: string;
    name: string;
    email: string;
  };
  homework: {
    delivered: number;
    uploaded: number;
  };
}

export interface CourseTeacher {
  id: number;
  code: string;
  banner: string;

  name: string;
  description: string;

  teacher: {
    id: number;
    picture: string;
    name: string;
    email: string;
  };

  homeworks: number;
  forums: number;
  students: number;
}

// export interface HomeworkView {
//   id: number;
//   name: string;
//   description: String;
//   deliveryDate: Date;
//   status: boolean;
// }

// export interface HomeworkData {
//   id: number;
//   name: string;
//   uploadDate: Date;
//   deliveyDate: Date;
//   description: string;
//   maxCalification: number;
//   upload?: {
//     id: number;
//     studentCode: number;
//     deliveryDate: Date;
//     calification: number;
//     files: File[];
//   };
//   files: File[];
// }

export interface User {
  code: number;
  name: string;
  firstLastName: string;
  secondLastName: string;
  email: string;
  profileImage: string;
  activationStatus: boolean;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
}

// <---------------------------------------------------------------------------------------------------------------------------------------------------------------------------->

//Todos los usuarios
export interface Profile {
  code: number;
  name: string;
  firstLastname: string;
  secondLastname: string;
  profileImage: string;
  email: string;
}

export interface Student {
  code: number;
  name: string;
  firstLastname: string;
  secondLastname: string;
  profileImage: string;
  calification: number;
  delivered: number;
}

// Alumno/Profesor
export interface CoursePreView {
  id: number;
  code: string;
  name: string;
  banner: string;
  teacher: Profile;
  // Diferencia de tareas subidas a entrehadas
  difference: number;
}

export interface CoursePreviewT {
  id: number;
  code: string;
  name: string;
  banner: string;
}

//Admin
export interface CoursePreViewAdmin {
  id: number;
  code: string;
  teacher: {
    code: number;
    name: string;
    firsLastname: string;
    seconLastname: string;
  };
}

// Alumno
export interface CourseView {
  id: number;
  name: string;
  banner: string;
  description: string;
  teacher: Profile;
  homeworks: number;
  forums: number;
  calification: number;
  h: NestedHomework;
}

export interface NestedHomework extends Paginable {
  previews: HomeworkPreview[];
  homeworks: {
    [id: number]: SHomeworkView;
  };
}

// Profesor / admin
export interface CourseInformation {
  id: number;
  code: string;
  name: string;
  description: string;
  // teacher: Profile;
  participants: number;
  homeworksUploades: number;
  forums: number;
}

export interface File {
  homeworkId: number;
  userCode: number;
  fileName: string;
  mimeType: string;
}

export interface Homework {
  id: number;
  name: string;
  description: string;
  maxCalification: number;
  uploadDate: Date;
  deliveryDate: Date;
  files?: File[];
}

export interface homeworkDelivered {
  id: number;
  studentCode: number;
  deliveryDate: Date;
  calification: number;
  files: File[];
}

// Alumno / Maestro / Admin
export interface HomeworkPreview {
  id: number;
  name: string;
  description: string;
  deliveryDate: Date;
  // Si ya fue entregada o si ya fueron revisados todos
  status: boolean;
}

export interface HomeworkCalification {
  data: homeworkDelivered;
  student: {
    code: number;
    name: string;
    firstLastname: string;
    secondLastname: string;
  };
}

// Alumno / Maestro / Admin
export interface HomeworkView {
  data: Homework;
  uploads?: HomeworkCalification[];
}

export interface SHomeworkView {
  id: number;
  name: string;
  description: string;
  deliveryDate: Date;
  maxCalification: number;
  files: HFile[];
  upload?: {
    calification: number;
    files: HFile[];
  };
}

export interface HFile {
  fileName: string;
  originalName: string;
  mimeType: string;
}

// Alumno / Maestro / Admin
export interface Forum {
  id: number;
  description: string;
  creation: Date;
  comment?: number;
}

export interface ForumPreView {
  id: number;
  description: string;
  creation: Date;
}

export interface Comment {
  id: number;
  forumId: number;
  description: string;
  author: {
    code: number;
    profileImage: string;
    name: string;
    firstLastname: string;
    secondLastname: string;
  };
}

export interface Nombre {}

export interface Nombre {}

export type UserType = '1' | '2' | '3';

export interface Route {
  meta: {
    requiresAuth: boolean | undefined;
    for?: 'administrator' | 'teacher' | 'student';
  };
}

export interface Field {
  value: string;
  errors: string[];
}

export interface AuthModule {
  loaded: boolean;
  token: string | null;
  user: {
    loaded: boolean;
    profile: User;
  };
  type: UserType | null;
}

export interface Paginable {
  page: number;
  perPage: number;
  remained: boolean;
  initialFetch: boolean;
}

export interface CourseModule extends Paginable {
  previews: CoursePreView[];
  courses: {
    [id: number]: CourseView;
  };
}

export interface CourseTModule extends Paginable {
  previews: CoursePreviewT[];
  courses: {
    [id: number]: CourseTeacher;
  };
}

export interface HomeworkModule {
  perPage: number;
  previews: {
    [id: number]: {
      data: HomeworkPreview[];
      page: number;
      initialFetch: boolean;
      remained: boolean;
    };
  };
}

export interface MessageModule {
  messages: PopperMessage[];
}

export interface StoreDefinition {
  auth: AuthModule;
  scourse: CourseModule;
  message: MessageModule;
}

export interface PopperMessage {
  id: string;
  level: 'error' | 'warning' | 'success';
  message: string;
  title: string;
  timeout: number;
}
