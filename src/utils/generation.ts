const ID = () => {
  const r = Math.random()
    .toString(36)
    .substr(2, 9);

  return `_${r}`;
};

export default ID;
