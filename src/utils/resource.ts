import config from '@/services';
import store from '@/store';
import { User } from '@/interfaces';

// export function user(): User {
//   return store.state.auth.user.profile;
// }

export function profileImage(user: User): string {
  const { token } = store.state.auth;
  return `${config.base}/sh/resource?type=profile&who=${user.code}&fileName=${user.profileImage}&token=${token}`;
}

export function banner(): string {
  const { token } = store.state.auth;
  return `Laloselacome &token=${token}`;
}

export default { profileImage };
