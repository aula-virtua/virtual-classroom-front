export function format(date: Date): string {
  const year = date.getFullYear();
  // const month = date.getMonth();
  // const day = date.getDate();

  let month = (date.getMonth() + 1).toString();
  let day = date.getDate().toString();
  if (parseInt(day, 10) < 10) day = `0${day}`;
  if (parseInt(month, 10) < 10) month = `0${month}`;

  return `${day}/${month}/${year}`;
}

export function formatHour(date: Date): string {
  let hours = date.getHours().toString();
  let minutes = date.getMinutes().toString();

  if (parseInt(hours, 10) < 10) hours = `0${hours}`;
  if (parseInt(minutes, 10) < 10) minutes = `0${minutes}`;

  return `${hours}:${minutes}`;
}

export default {
  format,
  formatHour,
};
