import { UserType } from '@/interfaces';

export const toBaseURL = (type: UserType): string => {
  if (type === '1') {
    return '/a';
  }

  if (type === '2') {
    return '/t';
  }

  return '/s';
};

export default {
  toBaseURL,
};
