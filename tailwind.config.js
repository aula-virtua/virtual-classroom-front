module.exports = {
  theme: {
    fontFamily: {
      main: ['"Raleway"'],
      secondary: ['"Roboto"'],
    },
    extend: {
      colors: {
        blue: '#04baf2',
      },
      inset: {
        full: '100%',
        '1': '1rem',
        '2': '2rem',
        '6': '6rem',
      },
      opacity: {
        10: '0.1',
        90: '0.9',
      },
      spacing: {
        '80': '20rem',
        '128': '32rem',
        '140': '35rem',
        '216': '54rem',
      },
      colors: {
        secondary: '#48ccf5',
        accent: '#eaf4fc',
        'light-gray': '#ada7a9',
        'dark-gray': '#7f7578',
        negrito: '#1a202c',
      },
      borderRadius: {
        default: '0.5rem',
      },
    },
  },
  variants: {},
  plugins: [],
};
