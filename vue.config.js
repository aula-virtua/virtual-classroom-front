module.exports = {
  runtimeCompiler: true,
  configureWebpack: {
    resolve: {
      extensions: ['.vue'],
    },
    module: {
      rules: [
        {
          test: /\.html$/,
          loader: 'vue-template-loader',
          exclude: /index.html/,
          options: {
            transformToRequire: {
              img: 'src',
            },
          },
        },
        {
          test: /\.(png|jpe?g|gif)$/i,
          use: [
            {
              loader: 'file-loader',
            },
          ],
        },
      ],
    },
  },
};
